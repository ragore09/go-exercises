package main

import "fmt"

func main() {
	type Apple float64

	const appleVariable Apple = 0.123
	fmt.Println(Apple(2.666))



	x := 1
	p := &x
	fmt.Println(p)
	fmt.Println(*p) //*p is the variable that p points to
	*p = 2
	fmt.Println(x)

	var z int
	var y *int

	fmt.Println(z == 0, &z == y, y == nil)
	fmt.Printf("%08b - %08b", 8, 8 << 1)





}
