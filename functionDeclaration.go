package main

import "fmt"

func main() {
	const temp1, temp2 = 32.0, 313.0
	fmt.Printf("%.3fF are: %.3fC\n", temp1, fahrenheitToCelsius(temp1))
	fmt.Printf("%.3fF are: %.3fC\n", temp2, fahrenheitToCelsius(temp2))
}

func fahrenheitToCelsius(f float64) float64 {
	return (f-32) * 5 / 9
}