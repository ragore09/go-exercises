package main

import (
	"fmt"
)

type Node struct {
	value int
	next *Node
}

type List struct {
	head *Node
	tail *Node
}

func createList() *List {
	return new(List)
}

func emptyList(list *List) {
	list.tail = nil
	list.tail = nil
}

func isListEmpty(list *List) bool {
	if list.tail == nil && list.head == nil {
		return true
	}
	return false
}

func addElementAtFront(list *List, value int) {
	newNode := new(Node)
	newNode.value = value
	if isListEmpty(list) {
		newNode.next = nil
		list.tail = newNode
	} else {
		newNode.next = list.head
	}
	list.head = newNode
}

func addElementAtLast(list *List, value int) {
	newNode := new(Node)
	newNode.value = value
	newNode.next = nil
	if isListEmpty(list) {
		list.head = newNode
	} else {
		list.tail.next = newNode
	}
	list.tail = newNode
}

func printList(list *List) {
	node := list.head
	for node != nil {
		fmt.Printf("Value: %d\n", node.value)
		node = node.next
	}
}

func main() {
	list := createList()
	addElementAtFront(list, 4)
	addElementAtFront(list, 3)
	addElementAtFront(list, 2)
	addElementAtLast(list, 9)
	addElementAtLast(list, 8)
	addElementAtLast(list, 7)
	printList(list)
}

