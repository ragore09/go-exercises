package main

import (
	"encoding/json"
	"log"
	"fmt"
)

type user struct {
	UserName string
	FullName string
	Age int
	Email string
}

func main() {
	var someUser user
	someUser.UserName = "ragore09"
	someUser.FullName = "Raul Gonzalez"
	someUser.Age = 25
	someUser.Email = "ragore.09@gmail.com"
	data, err := json.Marshal(someUser)
	if err != nil {
		log.Fatalf("JSON marshaling failed: %s", err)
	}
	fmt.Printf("%s\n", data)
}
