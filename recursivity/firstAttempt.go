package main

import (
	"fmt"
	"regexp"
	"os"
)

var regex = regexp.MustCompile(`([^()][\s\w]+)*([(]+[\w\s]+[)]+)([\w\s()]+)*`)

func reverseString(phrase string) string {
	if len(phrase) == 0 {
		return phrase
	}
	chars := []rune(phrase)
	return reverseString(phrase[1:]) + string(chars[0])
}

func processString(phrase string, straight bool) string {
	tokens := regex.FindStringSubmatch(phrase)
	if len(tokens) == 0 {
		if straight {
			return phrase
		}
		return reverseString(phrase)
	} else {
		prefix, reverse, postfix := tokens[1], tokens[2], tokens[3]
		return prefix + processString(reverse[1: len(reverse)-1], !straight) + processString(postfix, straight)
	}
}

// ------------- SECOND ATTEMPT
func findParentheses(phrase string) (int, int) {
	firstIndex, lastIndex := -1, -1
	var firstFound, lastFound bool
	chars := []rune(phrase)
	for i1, i2 := 0, len(chars)-1; i1<len(chars); i1, i2 = i1+1, i2-1 {
		if chars[i1] == ')' && !lastFound {
			lastIndex = i1
			lastFound = true
		}
		if chars[i2] == '(' && !firstFound{
			firstIndex = i2
			firstFound = true
		}
	}
	return firstIndex, lastIndex
}

func processString2(phrase string) string {
	firstIndex, lastIndex := findParentheses(phrase)
	if firstIndex == -1 {
		return phrase
	}
	prefix, reverse, postfix := phrase[:firstIndex], phrase[firstIndex+1:lastIndex], phrase[lastIndex+1:]
	return processString2(prefix + reverseString(reverse) + postfix)
}

func main() {
	fmt.Println(processString2(os.Args[1]))
}
