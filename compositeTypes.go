package main

import (
	"fmt"
	"time"
)

func main() {

//	arrays
//	var numbers [5]int = [5]int{1, 2, 3, 4, 5}
	numbers := [5]int{1, 2, 3, 4, 5}
	fmt.Println(numbers[0])
	fmt.Println(numbers[len(numbers) - 1])

	for _, number := range numbers {
		fmt.Println(number)
	}

	type Currency int

	const (
		USD Currency = iota
		EUR
		GBP
		RMB
	)

	symbol := [...]string{USD: "$", EUR: "€", GBP: "£", RMB: "¥"}

	fmt.Println(RMB, symbol[RMB]) // "3 ¥"



//	maps!
	connectedUsers := make(map[string]User)

//	structs!

	type user struct {
		userKey, username, fullName string
		disconnected, isModerator, isReady bool
	}




}
