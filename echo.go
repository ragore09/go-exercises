package main

import (
	"fmt"
	"os"
)

// ------- initial version
//func main() {
//	var s, sep string
//	for i := 1; i < len(os.Args); i++ {
//		s += sep + os.Args[i]
//		sep = " "
//	}
//	fmt.Println(s)
//}

func main() {
	var s, sep string
	for index, arg := range os.Args[1:] {
		s += sep + arg
		sep = " "
		fmt.Printf("%d - %s\n", index, arg)
	}
}